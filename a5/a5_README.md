# LIS4381: Mobile Web Application Development

## Kyle Kane

### Assignment 5 # Requirements:


*Five Parts:*

1. Add server-side validation
2. All input fields are required except notes
3. Use regexp to allow only appropriate characters
4. Alter connection.php to connect to local petstore db
5. Create data table to store petstore db data


#### Assignment Screenshots:
*Screenshot of Index Page*:

![Index](/a5/img/index.png)

*Screenshot of Server-Side validation*:

![Validation](/a5/img/add_petstore.png)
