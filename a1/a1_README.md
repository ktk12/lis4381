# LIS4381: Mobile Web Application Development

## Kyle Kane

### Assignment 1 # Requirements:


*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development installations
3. Chapter questions


#### README.md file should include the following items:

* Screenshot of AMPPS running
* Screenshot of running java Hello
* Screenshot of Android Studio My First App running
* Git commands with short descriptions
* Bitbucket repo links: a) this assignment and b) the complete tutorial above(bitbucketstationlocation and 	team quotes)


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init: This command creates an empty Git repository
2. git status: This command displays the state of the working directory and the staging area
3. git add: This command adds file contents to the index
4. git commit: This command commits the staged snapshot to the project history
5. git push: This command sends your committed changes to bitbucket
6. git pull: This command pulls a file from a remote repo into your local repo
7. git clone: This command creates a copy of a local repository

#### Assignment Screenshots:

*Screenshot of AMPPS running*:

![AMPPS](a1/img/ampps_install.png)

*Screenshot of running java Hello*:

![JDK_INSTALL](/a1/img/jdk_install.png)

*Screenshot of Android Studio*:

![Android Studio](/a1/img/as_install.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/ktk12/bitbucketstationlocation)

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/ktk12/myteamquotes)
