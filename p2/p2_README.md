# LIS4381: Mobile Web Application Development

## Kyle Kane

### Project 2 # Requirements:


*Five Parts:*

1. Complete Web Application
2. Add Delete function
3. Add Edit function 



#### Assignment Screenshots:
*Screenshot of Index.php*:

![Index](/p2/img/index.png)

*Screenshot of First User Interface*:

![Edit](/p2/img/edit.png)

*Screenshot of Index.php*:

![Data_validation](/p2/img/data_validation.png)


