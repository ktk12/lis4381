# LIS4381: Mobile Web Application Development

## Kyle Kane

### Assignment 2 # Requirements:


*Six Parts:*

1. Further introduction into Andriod development
2. Complete Bruschetta Recipe andriod application
3. Create a "homepage" with multiple XML attributes
4. Link Recipe page to "homepage"
5. Extra credit for changing background color for both activities
6. Chapters Three and Four in Murach MySQL/PHP



#### Assignment Screenshots:

*Screenshot of Homepage running*:

![Screen1](/a2/img/screen1.png)

*Screenshot of Recipe information*:

![Screen2](/a2/img/screen2.png)
