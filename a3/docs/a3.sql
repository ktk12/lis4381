
DROP database if exists petstoreDB;

CREATE database if not exists petstoreDB;

use petstoreDB;

-- Customer Table
DROP table if exists customer;
CREATE table if not exists customer(
    cus_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    cus_fname VARCHAR(15) NOT NULL,
    cus_lname VARCHAR(30) NOT NULL,
    cus_street VARCHAR(30) NOT NULL,
    cus_city VARCHAR(30) NOT NULL,
    cus_state CHAR(2) NOT NULL,
    cus_zip INT UNSIGNED NOT NULL,
    cus_phone BIGINT UNSIGNED NOT NULL,
    cus_email VARCHAR(100) NOT NULL,
    cus_url VARCHAR(100) NULL,
    cus_balance DECIMAL(6,2) UNSIGNED NOT NULL,
    cus_total_sales DECIMAL(6,2) UNSIGNED NOT NULL,
    cus_notes VARCHAR(255) NULL,
    PRIMARY KEY (cus_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Petstore Table
DROP TABLE IF EXISTS petstore;
CREATE TABLE IF NOT EXISTS petstore(
    pst_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    pst_name VARCHAR(30) NOT NULL,
    pst_street VARCHAR(30) NOT NULL,
    pst_city VARCHAR(30) NOT NULL,
    pst_state CHAR(2) NOT NULL,
    pst_zip INT UNSIGNED NOT NULL,
    pst_phone BIGINT UNSIGNED NOT NULL,
    pst_email VARCHAR(100) NOT NULL,
    pst_url VARCHAR(100) NULL,
    pst_ytd_sales DECIMAL(9,2) UNSIGNED NOT NULL,
    pst_notes VARCHAR(255) NULL,
    PRIMARY KEY (pst_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Pet Table
DROP table if exists pet;
CREATE table if not exists pet(
    pet_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    cus_id SMALLINT UNSIGNED NOT NULL,
    pst_id SMALLINT UNSIGNED NOT NULL,
    pet_type VARCHAR(30) NOT NULL,
    pet_sex ENUM('m','f') NOT NULL,
    pet_price DECIMAL(7,2) UNSIGNED NOT NULL,
    pet_age SMALLINT UNSIGNED NOT NULL,
    pet_color VARCHAR(15) NOT NULL,
    pet_sale_date DATE NULL,
    pet_vaccine ENUM('y','n') NOT NULL,
    pet_neuter ENUM('y','n') NOT NULL,
    pet_notes VARCHAR(255) NULL,
    PRIMARY KEY(pet_id),

    CONSTRAINT fk_pet_customer
    FOREIGN KEY (cus_id)
    REFERENCES customer (cus_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,

    CONSTRAINT fk_pet_petstore
    FOREIGN KEY (pst_id)
    REFERENCES petstore (pst_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Customer Inserts
INSERT INTO customer
(cus_fname, cus_lname, cus_street, cus_city, cus_state, cus_zip, cus_phone, cus_email, cus_url, cus_balance, cus_total_sales, cus_notes)
VALUES
('Bobby', 'Monahan', '101 Main Street', 'New York City', 'NY', 10190, 3894859403, 'bmonahan@aol.com', NULL, 90.00, 105.00, NULL),
('Julie', 'Louie-Drefus', '101 Main Street', 'Pittsburgh', 'PA', 90394, 3894859403, 'jld@aol.com', NULL, 1000.00, 490.00, 'test'),
('Jerry', 'Seinfeld', 'Apt 21', 'New York City', 'NY', 10195, 8594039584, 'jseinfeld@aol.com', 'jerry.com', 20.50, 101.00, NULL),
('Kramer', 'K', 'Apt 22', 'New York City', 'NY', 10195, 4893094859, 'kram3r@aol.com', NULL, 900.00, 20.90, NULL),
('Bob', 'Newman', 'Apt 24', 'New York City', 'NY', 10195, 5894093843, 'Newmannnn@aol.com', NULL, 50.00, 23.00, NULL),
('Terry', 'Bradshaw', '120 Second Lane', 'Steel Town', 'PA', 94059, 5894093843, 'tbradshaw@aol.com', NULL, 50.00, 23.00, NULL),
('Dan', 'Marion', '100 Sunset Blvd', 'Miami', 'FL', 30549, 3059045948, 'MarionMAn@aol.com', NULL, 100.00, 15.00, NULL),
('Joe', 'Rogan', '904 Mountain Road', 'Hollywodd', 'CA', 10195, 1011011010, 'jre@aol.com', NULL, 500.00, 213.00, NULL),
('Horace', 'Gracie', '200 Choke you out Drive', 'Rio De Jeniro', 'BZ', 93040, 8490394322, 'choked@aol.com', NULL, 50.00, 23.00, NULL),
('Lonnie', 'Russel', 'Apt 24 Sunny side drive', 'San Fran', 'CA', 38943, 5894093843, 'Lrussel@aol.com', NULL, 50.00, 23.00, NULL);

-- Petstore Inserts
INSERT INTO petstore
(pst_name, pst_street, pst_city, pst_state, pst_zip, pst_phone, pst_email, pst_url, pst_ytd_sales, pst_notes)
VALUES
('Kmart', '101 Main Street', 'Edgewater', 'FL', 32141, 3863894859, 'kmartsupport@kmart.com', 'kmart.com', 190000.90, NULL),
('Petco', '101 Sunset Street', 'San Fran', 'CA', 10141, 9084904859, 'petcosupport@petco.com', 'petco.com', 101101.10, NULL),
('Critters Corner', '900 Dixie Highway', 'Edgewater', 'FL', 32369, 3863894859, 'ccorner@cc.com', 'cc.com', 25500.50, NULL),
('Furry Friends', '101 Main Street', 'Oakland', 'CA', 10141, 3863894859, 'furryfirendz@ff.com', 'ff.com', 20000.10, NULL),
('Pets R Us', '25 Furry Way', 'Tallahassee', 'FL', 32308, 8504903948, 'petsrus@ptrs.com', 'ptrs.com', 101900.90, NULL),
('Bestbuy', '1100 Going out of business', 'Tallahassee', 'FL', 32304, 8504897654, 'bestbuy@bestbuy.com', 'bestbuy.com', 101.00, NULL),
('Paws', '900 Joking Lane', 'Tallahassee', 'FL', 32309, 8509940394, 'pawsupports@paws.com', 'paws.com', 1090.00, NULL),
('Big Fish', '25 Scuba Drive', 'Edgewater', 'FL', 32368, 3863894859, 'bigfishpets@bfp.com', 'bfp.com', 25.00, 'test'),
('BigLots', '104 Tennessee Ave', 'Tallahassee', 'FL', 32304, 8504803945, 'BigLotssupport@BigLots.com', 'BigLots.com', 190000.00, NULL),
('PetMeUp', '202 Weird name road', 'San Jose', 'CA', 19093, 1093894895, 'PetMeUpsupport@PetMeUp.com', 'PetMeUp.com', 250000.00, NULL);

-- Pet Inserts
INSERT INTO pet
(cus_id, pst_id, pet_type, pet_sex, pet_price, pet_age, pet_color, pet_sale_date, pet_vaccine, pet_neuter, pet_notes)
VALUES
(1, 1, 'dog', 'm', 105.00, 22, 'brown', NULL, 'y', 'n', NULL),
(2, 2, 'dog', 'm', 35.00, 2, 'black', '2010-08-09', 'n', 'y', NULL),
(3, 3, 'camel', 'f', 305.00, 1, 'beige', '2009-01-01', 'y', 'n', NULL),
(4, 5, 'fish', 'f', 1222.00, 22, 'brown', NULL, 'n', 'y', NULL),
(6, 5, 'dog', 'm', 105.50, 22, 'brown', '2015-09-20', 'y', 'y', NULL),
(8, 10, 'fish', 'm', 98.00, 3, 'gold', '2006-01-15', 'y', 'n', NULL),
(1, 9, 'cat', 'm', 1000.00, 10, 'brown and yel', NULL, 'y', 'y', NULL),
(5, 1, 'tarantula', 'm', 50.50, 4, 'black', NULL, 'y', 'n', NULL),
(4, 3, 'human', 'f', 25.15, 23, 'white', '2016-12-01', 'n', 'n', NULL),
(7, 6, 'dog', 'm', 105.00, 5, 'yellow', NULL, 'y', 'n', NULL);

