# LIS4381: Mobile Web Application Development

## Kyle Kane TEST TEST TESTIES TESTies

### Assignment 3 # Requirements:


*Four Parts:*

1. Create Andriod MyEvent application
2. Use Java logic to obtain ticket number and calculate price
3. Create a Event homepage with functionality of ticket price
4. Chapters Five and Six in Murach MySQL/PHP



#### Assignment Screenshots:
*Screenshot of Petstore ERD*:

![Screen1](/a3/img/a3ERD.png)

*Screenshot of First User Interface*:

![Screen1](/a3/img/screen1.png)

*Screenshot of Second User Interface*:

![Screen2](/a3/img/screen2.png)

*Link to Petstore DB file*:

[Petstore DB File](/a3/docs/a3.sql)

*Link to a3 SQL problems file*:

[SQL Problems file](/a3/docs/a3SQL.sql)
