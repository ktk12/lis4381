
# LIS4381: Mobile Web Application Development

## Kyle Kane

### LIS4381 # Requirements:

####Course Work Links:

1. [A1 README.md](/a1/a1_README.md)

	* Install AMPPS
	* Install JDK
	* Install Android Stuido and create My First App
	* Provide screenshots of all installations
	* Create bitbucket repo and complete tutorials
	* Provide git commands descriptions
	

	
2.	[A2 README.md](/a2/a2_README.md)

	* Read Chapter 3 and 4 in Murach's MySQL/PHP
	* Follow Chapter 2 in Andriod Development Book
	* Build Bruschetta Recipe Application
	* Create link between both the homepage and recipe information
	


3.	[A3 README.md](/a3/a3_README.md)

	* Read Chapters 5 and 6 in Murachs MySQL/PHP
	* Complete Chapter 3 Project in Android Development Book
	* Build My Event app
	* Functionality inlcludes Spinner class with ticket price calculation


4.	[A4 README.md](/a4/a4_README.md)

	* Clone starter files in AMPPS/www
	* Use server side data validation
	* Use client side data validation
	* Learn about bootstrap carousel and add three images to the carousel
	* Chapters Nine, Ten and Fifteen in Murach MySQL/PHP
	* Include screenshots of main page, correct data and failed data


5.	[A5 README.md](/a5/a5_README.md) 
	
	* Add server-side validation
	* All input fields are required except notes
	* Use regexp to allow only appropriate characters
	* Alter connection.php to connect to local petstore db
 	* Create data table to store petstore db data

6.	[P1 README.md](/p1/p1_README.md)

	* Create My Business Card application
	* Create two page android application
	* Edit strings XML file to include all neccessary variables
	* Create button to link to second page
	* Include on first page Name, Image, and Button
	* Include on second page Name, Contact info, Interests

7.	[P2 README.md](/p2/p2_README.md)
	
	* Last class Jowett1. 
	* Complete Web Application
	* Add Delete function
 	* Add Edit function 
	
