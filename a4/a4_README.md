# LIS4381: Mobile Web Application Development

## Kyle Kane

### Assignment 4 # Requirements:


*Four Parts:*

1. Clone starter files in AMPPS/www
2. Use Server side and client side data validation in model
3. Update bootstrap carousel with three images linking to outside dev work or something
4. Chapters Nine, Ten and Fifteen in Murach MySQL/PHP



#### Assignment Screenshots:
*Screenshot of Main Page*:

![Main Page](/a4/img/main_page.png)

*Screenshot of Failed Validation*:

![Failed](/a4/img/failed.png)

*Screenshot of Correct Validation*:

![Correct](/a4/img/correct.png)
